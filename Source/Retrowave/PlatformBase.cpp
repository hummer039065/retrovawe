// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformBase.h"

#include "PlatformGenerator.h"
#include "RetrowaveCharacter.h"
#include "RetrowaveGameMode.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlatformBase, All, All);

// Sets default values
APlatformBase::APlatformBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(Scene);

	Platform = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	Platform->SetupAttachment(Scene);

	ConnectZonePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ZoneConnector"));
	ConnectZonePoint->SetupAttachment(Scene);

	EndZonePoint = CreateDefaultSubobject<UBoxComponent>(TEXT("EndZonePoint"));
	EndZonePoint->SetupAttachment(Scene);
	
	EndZonePoint->SetGenerateOverlapEvents(true);
	EndZonePoint->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	SpawnPoint->SetupAttachment(Platform);

}

// Called when the game starts or when spawned
void APlatformBase::BeginPlay()
{
	EndZonePoint->OnComponentBeginOverlap.AddDynamic(this, &APlatformBase::OnComponentBeginOverlap);
	
	Super::BeginPlay();
	
}

// Called every frame
void APlatformBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlatformBase::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	const auto Player = Cast<ARetrowaveCharacter>(OtherActor);
	if (!Player) return;

	const auto PlatformGenerator = Cast<APlatformGenerator>(GetOwner());
	
}

