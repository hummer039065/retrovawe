// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformGenerator.h"

#include "BotCharacter.h"
#include "PlatformBase.h"
#include "RetrowaveGameMode.h"
#include "Components/ArrowComponent.h"
#include "PickUp/PickUpMaster.h"


DEFINE_LOG_CATEGORY_STATIC(LogPlatformGenerator, All, All);

// Sets default values
APlatformGenerator::APlatformGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void APlatformGenerator::CreatePlatform(int32 Length)
{
	
	
	for (int32 i = 0; i < Length; i++)
	{
		auto PlatformRandomType = PlatformTypes[FMath::RandRange(0, PlatformTypes.Num() - 1)].Get();
		
		if (i == 0)
		{
			NextSpawnPoint = GetActorLocation();
			PlatformRandomType = StartPlatformClass;
		}

		if (i == Length - 1)
		{
			PlatformRandomType = FinishPlatformClass;
		}
		PlatformBase = Cast<APlatformBase>(GetWorld()->SpawnActor(PlatformRandomType));
		
		PlatformBase->SetOwner(this);
		PlatformBase->SetActorLocation(NextSpawnPoint);
		PlatformsArray.Add(PlatformBase);
		
		if (FMath::RandBool())
		{
			SpawnBonus();
		}

		if (bSpawnEnemies)
		{
			FMath::RandBool() ? SpawnEnemies() : 0;
		}
		
		NextSpawnPoint = PlatformsArray[i]->ConnectZonePoint->GetComponentLocation();
	}
}

bool APlatformGenerator::SpawnBonus()
{
	if (MaxBonusesOnLevel > 0 && PlatformBase->bCanPlaceBonus)
	{
		auto BonusRandomType = PickUpTypes[FMath::RandRange(0, PickUpTypes.Num() - 1)].Get();
		
		PickUp = Cast<APickUpMaster>(GetWorld()->SpawnActor(BonusRandomType));
		PickUp->SetActorLocation((PlatformBase->SpawnPoint->GetComponentLocation()) + FVector(0, 0, 70.0f));
		MaxBonusesOnLevel--;
	}
	
	return MaxBonusesOnLevel > 0 ? true : false;
}

bool APlatformGenerator::SpawnEnemies()
{
	if (MaxEnemiesOnLevel > 0 && PlatformBase->bCanPlaceEnemies)
	{
		const auto EnemyBot = Cast<ABotCharacter>(GetWorld()->SpawnActor(EnemyClass));
		if (EnemyBot)
		{
			EnemyBot->SetActorLocation((PlatformBase->SpawnPoint->GetComponentLocation()) + FVector(0, 0, 120.0f));
		}
		MaxEnemiesOnLevel--;
	}
	
	return MaxEnemiesOnLevel > 0 ? true : false;
}

// Called when the game starts or when spawned
void APlatformGenerator::BeginPlay()
{
	Super::BeginPlay();
	
	const auto GameMode = Cast<ARetrowaveGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		LineLength = GameMode->GetLinesLength();
		MaxBonusesOnLevel = GameMode->GetBonuses();
		MaxEnemiesOnLevel = GameMode->GetEnemiesOnLevel();
		bSpawnEnemies = GameMode->GetAgreeOnSpawnEnemies();
	}
}

// Called every frame
void APlatformGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

