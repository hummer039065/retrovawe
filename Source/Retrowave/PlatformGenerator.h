// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlatformGenerator.generated.h"

class ABotCharacter;
class APickUpMaster;
class APlatformBase;

UCLASS()
class RETROWAVE_API APlatformGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformGenerator();

	UPROPERTY()
	APlatformBase* PlatformBase;

	void CreatePlatform(int32 Length);
	
	bool SpawnBonus();

	bool SpawnEnemies();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<APlatformBase> PlatformClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<APlatformBase> StartPlatformClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<APlatformBase> FinishPlatformClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	/** How many platforms will be generated on level before end of level */
	int32 LineLength;
	
	/** How many Bonuses will be spawned on level per line */
	int32 MaxBonusesOnLevel;

	bool bSpawnEnemies;
	
	/** How many Enemies will be spawned on level per line */
	int32 MaxEnemiesOnLevel;

	/** Platform Types (Static, Moving, Destroyable, etc.) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="PlatformSettring")
	TArray<TSubclassOf<APlatformBase>> PlatformTypes;

	/** Array of created platforms */
	TArray<APlatformBase*> PlatformsArray;

	UPROPERTY()
	APickUpMaster* PickUp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="PlatformSettring")
	TSubclassOf<APickUpMaster> PickUpMasterClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="PlatformSettring")
	TArray<TSubclassOf<APickUpMaster>> PickUpTypes;

	UPROPERTY()
	ABotCharacter* Enemy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="PlatformSettring")
	TSubclassOf<ABotCharacter> EnemyClass;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<APlatformBase*> GetPlatformArray() const { return PlatformsArray; }

	FVector NextSpawnPoint;

private:
	

};
