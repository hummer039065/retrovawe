// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "RetrowaveCharacter.generated.h"

class UWeaponComponent;
class ABaseWeapon;
class UHealthComponent;
class UMainHUDWidget;


UCLASS(config=Game)
class ARetrowaveCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
public:
	ARetrowaveCharacter();

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY()
	UDecalComponent* CurrentCursor = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UMaterialInstance* CursorMaterial;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FVector CursorSize = FVector(20, 20, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UHealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWeaponComponent* WeaponComponent;
	
	void Aim(float DeltaSeconds);

	/** Using for test damage taken */
	void SelfDamage();

	//Weapon
	
	UPROPERTY()
	ABaseWeapon* CurrentWeapon;

protected:

	UPROPERTY()
	UMainHUDWidget* MainHUDWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="HUD")
	TSubclassOf<UMainHUDWidget> MainHUDWidgetClass;
	
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);
	
	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void BeginPlay() override;

	void CheckStatus();

	UFUNCTION()
	virtual void OnHealthChange(float Health);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	
	void OnDeath();




	
};

