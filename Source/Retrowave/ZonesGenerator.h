// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZonesGenerator.generated.h"

class APlatformGenerator;
class ALinesGenerator;

UCLASS()
class RETROWAVE_API AZonesGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZonesGenerator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	int32 ZonesCount;

	int32 ZoneLineLength;
	
	UPROPERTY()
	APlatformGenerator* PlatformGen;

	UPROPERTY()
	ALinesGenerator* LinesGenerator;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ALinesGenerator> LinesGeneratorClass;

	void CreateZones(int32 Count);

public:	


};


