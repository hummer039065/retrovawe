// Fill out your copyright notice in the Description page of Project Settings.


#include "LinesGenerator.h"
#include "PlatformBase.h"
#include "PlatformGenerator.h"
#include "RetrowaveCharacter.h"
#include "RetrowaveGameMode.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogLevelGenerator, All, All);

// Sets default values
ALinesGenerator::ALinesGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scewne"));
	SetRootComponent(Scene);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(Scene);

	Line_1 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line1"));
	Line_1->SetupAttachment(Scene);
	SpawnPoints.Add(Line_1);

	Line_2 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line2"));
	Line_2->SetupAttachment(Scene);
	SpawnPoints.Add(Line_2);

	Line_3 = CreateDefaultSubobject<UArrowComponent>(TEXT("Line3"));
	Line_3->SetupAttachment(Scene);
	SpawnPoints.Add(Line_3);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->SetupAttachment(Scene);

}

void ALinesGenerator::GenerateLevel(int32 Lines)
{
	for (int32 j = 0; j < Lines; j++)
	{
		PlatformGenerator = Cast<APlatformGenerator>(GetWorld()->SpawnActor(PlatformGeneratorClass));
		if (PlatformGenerator)
		{
			PlatformGenerator->SetActorLocation(SpawnPoints[j]->GetComponentLocation());
			PlatformGenerator->NextSpawnPoint = SpawnPoints[j]->GetComponentLocation();
			PlatformGenerator->CreatePlatform(ZoneLength);
		}
	}
}

// Called when the game starts or when spawned
void ALinesGenerator::BeginPlay()
{
	const auto GameMode = Cast<ARetrowaveGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		ZoneLength = GameMode->GetLinesLength();
	}

	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ALinesGenerator::ReachedPoint);

	Super::BeginPlay();
}

void ALinesGenerator::ReachedPoint(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	const auto Player = Cast<ARetrowaveCharacter>(OtherActor);
	if (Player)
	{
		if (IsFinish)
		{
			const auto GM = Cast<ARetrowaveGameMode>(GetWorld()->GetAuthGameMode());
			if (GM)
			{
				GM->GameOver();
			}
		}
		else
		{

		}
	}
}
