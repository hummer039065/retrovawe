// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlatformBase.generated.h"

class UBoxComponent;
class UArrowComponent;
UCLASS()
class RETROWAVE_API APlatformBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USceneComponent* Scene;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* Platform;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UArrowComponent* ConnectZonePoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBoxComponent* EndZonePoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UArrowComponent* SpawnPoint;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PlatformSettings")
	bool bCanPlaceBonus = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="PlatformSettings")
	bool bCanPlaceEnemies = false;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:

	

};
