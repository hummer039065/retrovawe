// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RetrowaveGameMode.generated.h"

UENUM()
enum class EMatchState : uint8
{
	WaitingToStart,
	InProgress,
	GameOver
};

DECLARE_MULTICAST_DELEGATE_OneParam(OnMatchStateChanged, EMatchState)


UCLASS(minimalapi)
class ARetrowaveGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARetrowaveGameMode();

	OnMatchStateChanged OnMatchStateChanged;

	UFUNCTION(BlueprintCallable)
	int32 GetZones() const { return Zones; }
	
	UFUNCTION(BlueprintCallable)
	int32 GetLines() const { return LinesPerZone; }
	
	UFUNCTION(BlueprintCallable)
	int32 GetLinesLength() const { return LinesLength; }

	UFUNCTION(BlueprintCallable)
	int32 GetBonuses() const { return BonusesInRound; }
	
	UFUNCTION(BlueprintCallable)
	bool GetAgreeOnSpawnEnemies() const { return IsSpawnEnemy; }
	
	UFUNCTION(BlueprintCallable)
	int32 GetEnemiesOnLevel() const { return EnemyCountInRound; }

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<UUserWidget> GameOveWidgetClass;

	void GameOver();

protected:

	virtual void BeginPlay() override;

private:
	EMatchState MatchState = EMatchState::WaitingToStart;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "2", ClampMax = "5"))
	int32 Zones = 2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "1", ClampMax = "3"))
	int32 LinesPerZone = 3;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "3", ClampMax = "45")) // Replace min clamp on 15 or 10
	int32 LinesLength = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "3", ClampMax = "10"))
	int32 BonusesInRound = 10;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"))
	bool IsSpawnEnemy = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="GameSettings", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "3", ClampMax = "10"), meta = (EditCondition = "IsSpawnEnemy"))
	int32 EnemyCountInRound = 10;
	
	void SetMatchState(EMatchState State);
	
};



