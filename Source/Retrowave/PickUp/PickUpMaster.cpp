// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/PickUp/PickUpMaster.h"
#include "Components/SphereComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpMaster, All, All);

// Sets default values
APickUpMaster::APickUpMaster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(Scene);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(Scene);
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SphereComponent->SetupAttachment(Scene);
	SphereComponent->SetSphereRadius(60.0f);
	

}

// Called when the game starts or when spawned
void APickUpMaster::BeginPlay()
{
	Super::BeginPlay();

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &APickUpMaster::OnPickUp);
	
}

// Called every frame
void APickUpMaster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUpMaster::OnPickUp(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	Destroy();
}
