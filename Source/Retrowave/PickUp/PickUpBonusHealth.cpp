// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/PickUp/PickUpBonusHealth.h"

#include "Retrowave/RetrowaveCharacter.h"
#include "Retrowave/Components/HealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBonusHealth, All, All);

void APickUpBonusHealth::OnPickUp(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	const auto Player = Cast<ARetrowaveCharacter>(OtherActor);
	if (Player)
	{
		auto CurrentHealth = Player->HealthComponent->GetHealth();
		const auto PlayerBonusMaxHealth = Player->HealthComponent->GetBonusMaxHealth();
		
		if (Player->HealthComponent->GetBonusMaxHealth() > (CurrentHealth + BonusHealth))
		{
			Player->HealthComponent->SetHealth(FMath::Clamp(CurrentHealth + BonusHealth, 0.0f, PlayerBonusMaxHealth));
			Destroy();
		}
	}
	
}
