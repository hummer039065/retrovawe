// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Retrowave/PickUp/PickUpMaster.h"
#include "PickUpBonusHealth.generated.h"

/**
 * 
 */
UCLASS()
class RETROWAVE_API APickUpBonusHealth : public APickUpMaster
{
	GENERATED_BODY()

public:

	virtual void OnPickUp(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

private:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float BonusHealth = 25.0f;
	
};
