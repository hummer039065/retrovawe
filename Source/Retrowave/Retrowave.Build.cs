// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Retrowave : ModuleRules
{
	public Retrowave(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "DeveloperSettings", "Navmesh", "NavigationSystem", "BehaviorTreeEditor", "GameplayTasks", "Niagara", "NiagaraCore" });
		
		PublicIncludePaths.AddRange(new string[] { "Retrowave/Components", "Retrowave/PickUp", "Retrowave/UI", "Retrowave/Weapons", "Retrowave/AI"});
	}
}
