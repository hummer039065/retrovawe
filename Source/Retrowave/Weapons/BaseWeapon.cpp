// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/Weapons/BaseWeapon.h"

#include "AIPerceptionComp.h"
#include "BotController.h"
#include "DrawDebugHelpers.h"
#include "HealthComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "VFXComponent.h"
#include "Components/DecalComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Retrowave/RetrowaveCharacter.h"
#include "NiagaraComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon, All, All);



// Sets default values
ABaseWeapon::ABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);

	VFXComponent = CreateDefaultSubobject<UVFXComponent>(TEXT("FXComponent"));

}

void ABaseWeapon::StartFire()
{
	MakeShot();
	GetWorldTimerManager().SetTimer(ShotTimer, this, &ABaseWeapon::MakeShot, RateOfFire, true);
}

void ABaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimer);
}

float ABaseWeapon::AmmoChange()
{
	return CurrentAmmo;
}


// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmo = Ammo;
}

void ABaseWeapon::MakeShot()
{
	if (IsAmmoEmpty() && bIsReloading)
	{
		StopFire();
		return;
	}
	
	const auto Character = Cast<ARetrowaveCharacter>(GetOwner());
	if (!Character) return;
	
	auto SocketTransform = WeaponMesh->GetSocketTransform("Barrel");

	FVector ShootLocation;
	FVector TargetLocation;
	
	if (Character->IsPlayerControlled())
	{
		ShootLocation = SocketTransform.GetLocation();
		TargetLocation = Character->CurrentCursor->GetComponentLocation();
	}
	else
	{
		const auto AIController = Cast<ABotController>(Character->GetOwner());
		if (!AIController) return;
		
		const auto PerceptionComp = Cast<UAIPerceptionComp>(AIController->GetPerceptionComponent());
		if (!PerceptionComp) return;
		
		ShootLocation = SocketTransform.GetLocation();
		TargetLocation = PerceptionComp->GetTargetLocation() + FVector(0.0f, (FMath::RandRange(50.0f, 200.0f)),(FMath::RandRange(10.0f, 100.0f)));
	}

	auto TraceFXEnd = TargetLocation;
	
	FHitResult Hit;
	auto TraceLine = GetWorld()->LineTraceSingleByChannel(Hit, ShootLocation, TargetLocation, ECollisionChannel::ECC_Visibility);
	
	if (TraceLine)
	{
		TraceFXEnd = Hit.ImpactPoint;
		MakeDamage(Hit);
		VFXComponent->PlayImpactFX(Hit);
	}

	SpawnTraceFX(ShootLocation, TraceFXEnd);
	SpawnImpactDecal(Hit);
	DecreaseAmmo();
}

void ABaseWeapon::MakeDamage(const FHitResult& Hit)
{

	auto DamagedActor = Hit.GetActor();
	if (!DamagedActor) return;
	
	if (DamagedActor->GetComponentByClass(UHealthComponent::StaticClass()))
	{
		DamagedActor->TakeDamage(DamageAmount, FDamageEvent{}, GetPlayerController(), this);
		UE_LOG(LogBaseWeapon, Display, TEXT("TakeDamage"));
	}
}

void ABaseWeapon::DecreaseAmmo()
{
	CurrentAmmo--;

	if (IsAmmoEmpty())
	{
		ChangeRound();
	}
}

bool ABaseWeapon::IsAmmoEmpty()
{
	return CurrentAmmo <= 0;
}

void ABaseWeapon::ChangeRound()
{
	UE_LOG(LogBaseWeapon, Display, TEXT("Reloading!"))
	StartReloading();
}

void ABaseWeapon::SetWeaponSettings(float Damage, float ROF, float AmmoInRound, float Reload)
{
	DamageAmount = Damage;
	RateOfFire = ROF;
	Ammo = AmmoInRound;
	ReloadTime = Reload;

	
}

void ABaseWeapon::StartReloading()
{
	bIsReloading = true;
	GetWorldTimerManager().SetTimer(ReloadTimer, this, &ABaseWeapon::StopReloading, ReloadTime, false);
}

void ABaseWeapon::StopReloading()
{
	bIsReloading = false;
	CurrentAmmo = Ammo;
	GetWorldTimerManager().ClearTimer(ReloadTimer);
	UE_LOG(LogBaseWeapon, Display, TEXT("CurrentAmmo: %f"), CurrentAmmo);
}

APlayerController* ABaseWeapon::GetPlayerController()
{
	const auto Player = Cast<ACharacter>(GetOwner());
	if (!Player) return nullptr; 

	return Player->GetController<APlayerController>();
}

void ABaseWeapon::SpawnTraceFX(FVector& StartLocation, FVector& EndLocation)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, StartLocation);
	if (TraceFXComponent)
	{
		TraceFXComponent->SetVariableVec3("TraceEnd", EndLocation);
	}
}

void ABaseWeapon::SpawnImpactDecal(FHitResult Hit)
{
	const auto Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),ImpactDecal, FVector(20.0f, 20.0f, 20.0f), Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
	if (Decal)
	{
		Decal->SetLifeSpan(3.0f);
	}
}
