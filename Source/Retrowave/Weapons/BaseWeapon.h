// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"


class UNiagaraSystem;
class UVFXComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAmmoChange);

UCLASS()
class RETROWAVE_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

	void SetWeaponSettings(float Damage, float ROF, float AmmoInRound, float Reload);
	
	virtual void StartFire();
	virtual void StopFire();

	UFUNCTION()
	float AmmoChange();

	float GetCurrentAmmo() const { return CurrentAmmo; }
	
	void ChangeRound();

	FTimerHandle GetReloadingTimer() const { return ReloadTimer; }
	float GetReloadTime() { return ReloadTime; }
	bool GetCheckReloading() { return bIsReloading; }

	void SpawnTraceFX(FVector& StartLocation, FVector& EndLocation);
	void SpawnImpactDecal(FHitResult Hit);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	APlayerController* GetPlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	USkeletalMeshComponent* WeaponMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	UVFXComponent* VFXComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	float DamageAmount = 10.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	float RateOfFire = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	float Ammo = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	float ReloadTime = 3.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	UNiagaraSystem* TraceFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	UMaterialInterface* ImpactDecal;
	
	void MakeShot();
	void MakeDamage(const FHitResult& Hit);

	void DecreaseAmmo();
	bool IsAmmoEmpty();
	
	UPROPERTY(BlueprintReadWrite)
	bool bIsReloading = false;
	
	void StartReloading();
	void StopReloading();

private:
	
	FTimerHandle ShotTimer;
	FTimerHandle ReloadTimer;

	float CurrentAmmo;

	


};
