// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/Weapons/VFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Niagara/Classes/NiagaraSystem.h"

// Sets default values for this component's properties
UVFXComponent::UVFXComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UVFXComponent::PlayImpactFX(const FHitResult& Hit)
{
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ImpactFX, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
}
