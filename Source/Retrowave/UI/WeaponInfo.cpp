// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/UI/WeaponInfo.h"

#include "BaseWeapon.h"
#include "WeaponComponent.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Retrowave/RetrowaveCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponInfo, All, All);

float UWeaponInfo::GetAmmo() const
{

	const auto WeaponComponent = GetWeaponComponent();
	if (!WeaponComponent) return 0;

	const auto Weapon = WeaponComponent->GetCurrentWeapon();
	return Weapon->GetCurrentAmmo();
}

float UWeaponInfo::GetReloadingTime(bool &IsReloading) 
{
	const auto WeaponComponent = GetWeaponComponent();
	if (!WeaponComponent) return 0;

	const auto Weapon = WeaponComponent->GetCurrentWeapon();
	auto ReloadTime = Weapon->GetReloadTime(); 
	const auto ReloadTimerHandle = Weapon->GetReloadingTimer();
	auto ActiveReloading = Weapon->GetCheckReloading();
	auto Timer = UKismetSystemLibrary::K2_GetTimerElapsedTimeHandle(GetWorld(), ReloadTimerHandle) ;
	ActiveReloading ? IsReloading = true : IsReloading = false;
	
	return Timer / ReloadTime;
}

UWeaponComponent* UWeaponInfo::GetWeaponComponent() const
{
	const auto Player = Cast<ARetrowaveCharacter>(GetOwningPlayerPawn());
	if (!Player) return nullptr;

	const auto Component = Player->GetComponentByClass(UWeaponComponent::StaticClass());
	const auto AmmoInfo = Cast<UWeaponComponent>(Component);
	if (!AmmoInfo) return nullptr;

	return AmmoInfo;
}
