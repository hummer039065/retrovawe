// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "WeaponInfo.generated.h"

class UWeaponComponent;
class UTextBlock;
/**
 * 
 */
UCLASS()
class RETROWAVE_API UWeaponInfo : public UUserWidget
{
	GENERATED_BODY()


public:
	
	/*UPROPERTY(meta = (BindWidget))
	UTextBlock* AmmoText;*/

	UFUNCTION(BlueprintCallable)
	float GetAmmo() const;

	UFUNCTION(BlueprintCallable)
	float GetReloadingTime(bool &IsReloading);

	UWeaponComponent* GetWeaponComponent() const;

};
