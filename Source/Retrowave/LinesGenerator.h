// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LinesGenerator.generated.h"

class UBoxComponent;
class APlatformGenerator;
class UArrowComponent;

UCLASS()
class RETROWAVE_API ALinesGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALinesGenerator();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	USceneComponent* Scene;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	UArrowComponent* Line_1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	UArrowComponent* Line_2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	UArrowComponent* Line_3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="LevelSettings")
	UBoxComponent* Trigger;

	void GenerateLevel(int32 Lines);
	
	UPROPERTY()
	APlatformGenerator* PlatformGenerator;

	bool IsFinish = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** How many platforms will be generated on level before end of level */
	int32 ZoneLength;
	
	UFUNCTION()
	void ReachedPoint(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<APlatformGenerator> PlatformGeneratorClass;

	TArray<UArrowComponent*> SpawnPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ALinesGenerator> LevelGeneratorClass;
	
};
