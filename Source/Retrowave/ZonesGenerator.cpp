// Fill out your copyright notice in the Description page of Project Settings.


#include "ZonesGenerator.h"
#include "LinesGenerator.h"
#include "PlatformBase.h"
#include "PlatformGenerator.h"
#include "RetrowaveGameMode.h"

DEFINE_LOG_CATEGORY_STATIC(LogZonesGenerator, All, All);

// Sets default values
AZonesGenerator::AZonesGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AZonesGenerator::BeginPlay()
{
	Super::BeginPlay();

	const auto GameMode = Cast<ARetrowaveGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		ZonesCount = GameMode->GetZones();
		ZoneLineLength = GameMode->GetLines();
		CreateZones(ZonesCount);
	}
}


void AZonesGenerator::CreateZones(int32 Count)
{
	FVector NextPointSpawn;
	
	for (int32 i = 0; i <= Count; i++)
	{
		LinesGenerator = Cast<ALinesGenerator>(GetWorld()->SpawnActor(LinesGeneratorClass));
		if (LinesGenerator)
		{
			if (i == 0)
			{
				NextPointSpawn = GetActorLocation();
			}
		}
		
		LinesGenerator->SetActorLocation(NextPointSpawn);
		
		if (i < Count)
		{
			LinesGenerator->GenerateLevel(ZoneLineLength);

			PlatformGen = Cast<APlatformGenerator>(LinesGenerator->PlatformGenerator);
			if (PlatformGen)
			{
				const auto Platforms = PlatformGen->GetPlatformArray();
				const auto NextZonePoint = Platforms[Platforms.Num() - 1]->GetActorLocation().X;
				NextPointSpawn = GetActorLocation() + FVector(NextZonePoint + 500.0f, 0.0f, 0.0f);
			}
		}
		else
		{
			
			LinesGenerator->IsFinish = true;
		}
	}
}