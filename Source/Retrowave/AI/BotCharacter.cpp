// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/AI/BotCharacter.h"
#include "BotController.h"
#include "EnemyHealthBar.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"

ABotCharacter::ABotCharacter()
{
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ABotController::StaticClass();

	HealthBar = CreateDefaultSubobject<UWidgetComponent>("HealthBar");
	HealthBar->SetupAttachment(GetRootComponent());
	HealthBar->SetWidgetSpace(EWidgetSpace::Screen);
	
}

void ABotCharacter::OnHealthChange(float Health)
{
	const auto HealthBarWidget = Cast<UEnemyHealthBar>(HealthBar->GetUserWidgetObject());
	if (!HealthBarWidget) return;

	HealthBarWidget->SetHealthPercent(Health);

	//WidgetComponent



	

	
}

