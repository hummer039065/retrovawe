// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnemyHealthBar.generated.h"

class UProgressBar;
/**
 * 
 */
UCLASS()
class RETROWAVE_API UEnemyHealthBar : public UUserWidget
{
	GENERATED_BODY()

public:

	void SetHealthPercent(float Percent);
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* HealthBar;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MinHealthVisible = 0.9;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float ChangeHealthColorOnCritical = 0.3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FLinearColor HealthGoodColor = FLinearColor::Green;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FLinearColor HealthCriticalColor = FLinearColor::Red;
};
