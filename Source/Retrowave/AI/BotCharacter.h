// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Retrowave/RetrowaveCharacter.h"
#include "BotCharacter.generated.h"

class UWidgetComponent;
/**
 * 
 */
UCLASS()
class RETROWAVE_API ABotCharacter : public ARetrowaveCharacter
{
	GENERATED_BODY()

public:
	ABotCharacter();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UWidgetComponent* HealthBar;

	virtual void OnHealthChange(float Health) override;
};
