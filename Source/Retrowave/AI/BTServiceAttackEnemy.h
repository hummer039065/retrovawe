// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTServiceAttackEnemy.generated.h"

/**
 * 
 */
UCLASS()
class RETROWAVE_API UBTServiceAttackEnemy : public UBTService
{
	GENERATED_BODY()

public:
	UBTServiceAttackEnemy();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FBlackboardKeySelector Enemy;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};
