// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/AI/EnemyHealthBar.h"
#include "Components/ProgressBar.h"

void UEnemyHealthBar::SetHealthPercent(float Percent)
{
	if (!HealthBar) return;

	const auto HealthVisibility = Percent > MinHealthVisible || FMath::IsNearlyZero(Percent) ? ESlateVisibility::Hidden : ESlateVisibility::Visible;
	HealthBar->SetVisibility(HealthVisibility);

	const auto HealthColor = Percent > ChangeHealthColorOnCritical ? HealthGoodColor : HealthCriticalColor;
	HealthBar->SetFillColorAndOpacity(HealthColor);

	HealthBar->SetPercent(Percent);

	
}
