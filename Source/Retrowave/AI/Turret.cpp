// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/AI/Turret.h"

#include "HealthComponent.h"
#include "WeaponComponent.h"

// Sets default values
ATurret::ATurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(GetRootComponent());

	Barrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Barrel->SetupAttachment(Mesh);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	WeaponComponent = CreateDefaultSubobject<UWeaponComponent>(TEXT("Weapon"));*/

}

// Called when the game starts or when spawned
void ATurret::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

