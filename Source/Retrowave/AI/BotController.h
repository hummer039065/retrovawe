// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotController.generated.h"

class UAIPerceptionComp;

UCLASS()
class RETROWAVE_API ABotController : public AAIController
{
	GENERATED_BODY()

public:
	
	ABotController();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAIPerceptionComp* AIPerceptionComponent;

	virtual void Tick(float DeltaSeconds) override;

	
	
};
