// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/AI/BotController.h"

#include "AIPerceptionComp.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBotController, All, All);

ABotController::ABotController()
{
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComp>(TEXT("PerceptionComponent"));
	SetPerceptionComponent(*AIPerceptionComponent);
}

void ABotController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	const auto Aim = AIPerceptionComponent->GetEnemy();
	if (Aim)
	{
		const auto BlackboardComp = GetBlackboardComponent();
		if (BlackboardComp)
		{
			BlackboardComp->SetValueAsObject("Enemy", Aim);
		}
	}

}
