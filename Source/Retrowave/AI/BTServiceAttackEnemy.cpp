// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/AI/BTServiceAttackEnemy.h"

#include "AIController.h"
#include "AIPerceptionComp.h"
#include "WeaponComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBTSAttack, All, All);

UBTServiceAttackEnemy::UBTServiceAttackEnemy()
{
	NodeName = "AttackEnemy";
}

void UBTServiceAttackEnemy::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto AIController = OwnerComp.GetAIOwner();
	const auto Blackboard = OwnerComp.GetBlackboardComponent();

	//const auto HasTarget = Blackboard && Blackboard->GetValueAsObject(Enemy.SelectedKeyName);
	auto Target = Cast<UAIPerceptionComp>(AIController->GetAIPerceptionComponent());
	const auto HasTarget = Target->GetEnemy();

	if (AIController)
	{
		const auto WeaponComponent = Cast<UWeaponComponent>(AIController->GetPawn()->FindComponentByClass(UWeaponComponent::StaticClass()));
		if (WeaponComponent)
		{
			if (HasTarget)
			{
				WeaponComponent->StartFire();
				//UE_LOG(LogBTSAttack, Display, TEXT("Attacking!"));
			}
			else
			{
				WeaponComponent->StopFire();
				//UE_LOG(LogBTSAttack, Display, TEXT("JustLooking!"));
			}
		}
	}
	
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
