// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponComponent.generated.h"



class ABaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RETROWAVE_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponComponent();

	UFUNCTION(BlueprintCallable)
	void StartFire();
	UFUNCTION(BlueprintCallable)
	void StopFire();
	UFUNCTION(BlueprintCallable)
	void Reload();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ABaseWeapon> WeaponClass;

	UFUNCTION(BlueprintCallable)
	ABaseWeapon* GetCurrentWeapon() const { return CurrentWeapon; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY()
	ABaseWeapon* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSettings", meta = (AllowPrivateAccess = "true"))
	bool IsOverrideWeaponSettings = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSettings",  meta = (AllowPrivateAccess = "true"), meta=(EditCondition = "IsOverrideWeaponSettings"))
	float DamageAmount = 10.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSettings", meta = (AllowPrivateAccess = "true"), meta=(EditCondition = "IsOverrideWeaponSettings"))
	float RateOfFire = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSettings", meta = (AllowPrivateAccess = "true"), meta=(EditCondition = "IsOverrideWeaponSettings"))
	float Ammo = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSettings", meta = (AllowPrivateAccess = "true"), meta=(EditCondition = "IsOverrideWeaponSettings"))
	float ReloadTime = 3.0f;

	void SpawnWeapon();

};
