// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/Components/AIPerceptionComp.h"
#include "Perception/AISense_Sight.h"
#include "Retrowave/RetrowaveCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogAIPerceptionComp, All, All);

AActor* UAIPerceptionComp::GetEnemy()
{
	TArray<AActor*> PercieveActors;
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
	
	if (PercieveActors.Num() == 0) return nullptr;

	AActor* Target = nullptr;

	for (const auto PercieveActor : PercieveActors)
	{
		const auto Player = Cast<ARetrowaveCharacter>(PercieveActor);
		if (!Player) return nullptr;

		const auto PlayerController = Cast<APlayerController>(Player->GetController());
		if (!PlayerController) return nullptr;

		if (Player && PlayerController)
		{
			Target = Player;
		}
		
		if (Target)
		{
			TargetLocation = Target->GetActorLocation();
		}
	}
	return Target;
}

void UAIPerceptionComp::TickComponent(float DeltaTime, ELevelTick Tick, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, Tick, ThisTickFunction);
}

