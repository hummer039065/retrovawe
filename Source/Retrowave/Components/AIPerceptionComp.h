// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "AIPerceptionComp.generated.h"

UCLASS()
class RETROWAVE_API UAIPerceptionComp : public UAIPerceptionComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	AActor* GetEnemy();

	FVector GetTargetLocation() const { return TargetLocation; }
	

protected:
	
	FVector TargetLocation{FVector::ZeroVector};
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	

	
};
