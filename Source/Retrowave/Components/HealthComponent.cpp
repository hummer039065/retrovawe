// Fill out your copyright notice in the Description page of Project Settings.


#include "Retrowave/Components/HealthComponent.h"
#include "BaseWeapon.h"
#include "BotController.h"
#include "BrainComponent.h"
#include "Retrowave/RetrowaveCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::BeginPlay()
{
	SetHealth(MaxHealth);
	OnHealthChanged.Broadcast(Health);
	
	Super::BeginPlay();

	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeDamage);
	}
}



void UHealthComponent::OnTakeDamage(
	AActor* DamagedActor,
	float Damage,
	const UDamageType* DamageType,
	AController* InstigatedBy,
	AActor* DamageCauser)
{
	if (Damage <= 0.0f || IsDead()) return;

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	OnHealthChanged.Broadcast(Health);

	if (IsDead())
	{
		OnDeath.Broadcast();
		UE_LOG(LogHealthComponent, Display, TEXT("Character is Dead!"));
		
		const auto Character = Cast<ARetrowaveCharacter>(GetOwner());
		if (!Character) return;

		const auto Controller = Cast<ABotController>(Character->GetController());
		if (!Controller) return;
		
		if (Character)
		{
			Controller->GetBrainComponent()->Cleanup();
			Character->SetLifeSpan(5.0f);
			
		}
	}


}

