// Copyright Epic Games, Inc. All Rights Reserved.

#include "RetrowaveCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "RetrowaveGameMode.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Components/HealthComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "UI/MainHUDWidget.h"
#include "Weapons/BaseWeapon.h"
#include "WeaponComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogRetrowaveCharacter, All, All);



//////////////////////////////////////////////////////////////////////////
// ARetrowaveCharacter

ARetrowaveCharacter::ARetrowaveCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	//BaseTurnRate = 45.f;
	//BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 140.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

	WeaponComponent = CreateDefaultSubobject<UWeaponComponent>(TEXT("WeaponComponent"));

	
	

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ARetrowaveCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("CheckStatus", IE_Pressed, this, &ARetrowaveCharacter::CheckStatus);
	PlayerInputComponent->BindAction("TakeDamage", IE_Pressed, this, &ARetrowaveCharacter::SelfDamage);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, WeaponComponent, &UWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent, &UWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, WeaponComponent, &UWeaponComponent::Reload);

	PlayerInputComponent->BindAxis("MoveForward", this, &ARetrowaveCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARetrowaveCharacter::MoveRight);
	
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ARetrowaveCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ARetrowaveCharacter::TouchStopped);

}

void ARetrowaveCharacter::OnDeath()
{
	UE_LOG(LogRetrowaveCharacter, Display, TEXT("Character is dead!"));
	WeaponComponent->StopFire();
	
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
	
	if (this->IsPlayerControlled())
	{
		const auto GameMode = Cast<ARetrowaveGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			const auto MyController = GetController();
			if (MyController)
			{
				CurrentCursor->DestroyComponent();
				MyController->GetPawn()->Reset();
			}
			GameMode->RestartPlayer(MyController);
		}
		UE_LOG(LogRetrowaveCharacter, Display, TEXT("Try to Respawn"));
	}
	else
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		GetMesh()->SetSimulatePhysics(true);
	}
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
}


void ARetrowaveCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ARetrowaveCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ARetrowaveCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0, 0, 0));
	}

	MainHUDWidget = CreateWidget<UMainHUDWidget>(GetWorld(), MainHUDWidgetClass);
	if (MainHUDWidget)
	{
		MainHUDWidget->AddToViewport();
	}

	HealthComponent->OnDeath.AddUObject(this, &ARetrowaveCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &ARetrowaveCharacter::OnHealthChange);
	CurrentWeapon = WeaponComponent->GetCurrentWeapon();
	
	GetCharacterMovement()->Activate();
	GetCharacterMovement()->SetDefaultMovementMode();
	
}

void ARetrowaveCharacter::CheckStatus()
{
	UE_LOG(LogRetrowaveCharacter, Display, TEXT("CurrentHealth: %f"), HealthComponent->GetHealth());
}

void ARetrowaveCharacter::OnHealthChange(float Health)
{
	UE_LOG(LogRetrowaveCharacter, Display, TEXT("Character OnHealthChange"));
}

void ARetrowaveCharacter::Tick(float DeltaSeconds)
{
	Aim(DeltaSeconds);
	
	Super::Tick(DeltaSeconds);
}

void ARetrowaveCharacter::Aim(float DeltaSeconds)
{
	if (CurrentCursor)
	{
		APlayerController* PlayerController = Cast<APlayerController>(GetController());

		FHitResult Result;

		if (PlayerController)
		{
			
			//PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, Result);
			PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Result);
			FVector CursorVector = Result.ImpactNormal;
			FRotator CursorRotator = CursorVector.Rotation();

			CurrentCursor->SetWorldLocation(Result.Location);
			CurrentCursor->SetWorldRotation(CursorRotator);

			FVector WorldLocation;
			FVector WorldDirection;
			float DistanceAboveGround = 1.0f;

			PlayerController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection); 
			FVector PlaneOrigin(0.0f, 0.0f, DistanceAboveGround);
			FVector ActorWorldLocation = FMath::LinePlaneIntersection(WorldLocation, WorldLocation + WorldDirection, PlaneOrigin, FVector::UpVector);
	
			float RotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ActorWorldLocation).Yaw;
			SetActorRotation(FRotator(0.0, RotatorResult, 0.0));
		}
	}
}

void ARetrowaveCharacter::MoveForward(float Value)
{
	if (Value == 0) return;
	AddMovementInput(FVector(Value, 0, 0));
}

void ARetrowaveCharacter::MoveRight(float Value)
{
	if (Value == 0) return;
	AddMovementInput(FVector(0, Value, 0));
}

void ARetrowaveCharacter::SelfDamage()
{
	TakeDamage(10.0f, FDamageEvent{}, nullptr, this);
}

