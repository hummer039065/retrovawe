// Copyright Epic Games, Inc. All Rights Reserved.

#include "RetrowaveGameMode.h"
#include "RetrowaveCharacter.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/PawnMovementComponent.h"
#include "UObject/ConstructorHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogRetroGameMode, All, All);

ARetrowaveGameMode::ARetrowaveGameMode()
{
	// set default pawn class to our Blueprinted character

	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Player/Blueprints/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ARetrowaveGameMode::BeginPlay()
{
	Super::BeginPlay();

	SetMatchState(EMatchState::InProgress);
}

void ARetrowaveGameMode::GameOver()
{
	SetMatchState(EMatchState::GameOver);

	const auto GameOverWidget = CreateWidget<UUserWidget>(GetWorld(), GameOveWidgetClass);
	if (GameOverWidget)
	{
		GameOverWidget->AddToViewport();
	}
	
	const auto Character = Cast<ARetrowaveCharacter>(GetWorld());
	if (Character)
	{
		Character->GetMovementComponent()->Deactivate();
	}



	UE_LOG(LogRetroGameMode, Display, TEXT("Game OVer"));
	
}

void ARetrowaveGameMode::SetMatchState(EMatchState State)
{
	if (MatchState == State) return;

	MatchState = State;
	OnMatchStateChanged.Broadcast(MatchState);
}
